import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.mlab import psd, csd
from scipy import fftpack, signal
from tqdm import tqdm

time=np.loadtxt('impact_times.txt')
full=np.loadtxt('LPFdata/g2_huge_file_ifft.dat')
ltime=np.loadtxt('LPFdata/lisa_times.dat')

for i in range(time.size):
    pos=0

    curtime=time[i]
    try:
        pos=np.where(ltime=curtime)
        print(time[i], full[pos], ltime[pos])
    except:
        #print(curtime, "????")
        tmptime=100000000000
        tmpval=88
        for j in tqdm(range(ltime.size)):
            timediff=int(ltime[j])-int(curtime)
            if abs(timediff) < abs(tmptime):
                tmptime=abs(timediff)
                tmpval=j
        print("closest", curtime, tmptime, tmpval)


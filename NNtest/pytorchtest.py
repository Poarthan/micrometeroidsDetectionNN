import os, sys
import random
import pandas

n=1

file=open("open.csv","a")
file.truncate(0)
file.write("#")
file.write(',')
file.write("SepalLength")
file.write(',')
file.write("SepalWidth")
file.write(',')
file.write("PetalLength")
file.write(',')
file.write("PetalWidth")
file.write(',')
file.write("Iris_Type")
file.write("\n")


for i in range(1000):
    file.write(str(n))
    file.write(',')
    file.write(str(random.random()+random.randint(4,5)))
    file.write(',')
    file.write(str(random.random()+3))
    file.write(',')
    file.write(str(random.random()/3+1.22))
    file.write(',')
    file.write(str(random.random()/10+0.2))
    file.write(',')
    file.write("Iris-setosa")
    #file.write("0")
    file.write("\n")
    n+=1
    
for i in range(1000):
    file.write(str(n))
    file.write(',')
    file.write(str(random.random()*2+random.randint(4,5)+0.5))
    file.write(',')
    file.write(str(random.random()+2.5))
    file.write(',')
    file.write(str(random.random()/3))
    file.write(',')
    file.write(str(random.random()/10))
    file.write(',')
    file.write("Iris-versicolor")
    #file.write("1")
    file.write("\n")
    n+=1
    
for i in range(1000):
    file.write(str(n))
    file.write(',')
    file.write(str(random.random()+random.randint(4,5)-0.7))
    file.write(',')
    file.write(str(random.random()+3))
    file.write(',')
    file.write(str(random.random()/3))
    file.write(',')
    file.write(str(random.random()/10+0.27))
    file.write(',')
    file.write("Iris-virginica")
    #file.write("2")
    file.write("\n")
    n+=1
file.close()
